"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AppTitle = exports.AppTitle = function AppTitle(title) {
    _classCallCheck(this, AppTitle);

    this.title = "Haxor 1 - " + title + "Haxxxor 3";
    document.getElementById("title").innerHTML = this.title;
};